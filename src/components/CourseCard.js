import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

export default function CourseCard ({courseProp}) {
	//console.log(courseProp)
	const { name, description, price } = courseProp

	const [count, setCount ] = useState(0)
	//console.log(useState(0));
	const [seat, setSeat ] = useState(10)
	
	const [isDisabled, setIsDisabled ] = useState(false);

	function enroll() {
		if (seat > 0 ){
			setCount( count + 1);
			setSeat(seat - 1);
		} else {
			alert(`No More Seats Available`)
		}
		
	}

	//Use Effect(cb(), [dependency])
	useEffect(() =>{
		if(seat === 0) {
			setIsDisabled(true)
		}
		
	}, [seat])

	return (
		<Container fluid className="mb-4">
			<Row className="justify-content-center">
				<Col xs={10} md={8}>
					<Card className="p-4">
						<Card.Body>
							<Card.Title>
								Course Name: {name}
							</Card.Title>
							<Card.Text>
									Price: {price}
							</Card.Text>
							<Card.Subtitle> Description: {description}</Card.Subtitle>
							<Card.Text>Enrollees: {count}</Card.Text>
							<Card.Text>Seats Available: {seat}</Card.Text>
							<Button variant="primary" onClick={enroll} disabled={isDisabled}>Enroll</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}

//Check if the CourseCard component is getting the correct prop types
//Prototypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next.

CourseCard.propTypes = {
	//shape method => is used to check if a prop object conforms to a specific "shape"
	courseProp: PropTypes.shape({
		//Define the properties and their expected tpes
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
