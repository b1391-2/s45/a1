import { Container, Row, Col, Card } from "react-bootstrap";

export default function Highlights () {
	return (
		/*Card*/

		<Container fluid>
			<Row>
				<Col className="d-flex mb-3">
					<Card xs={12} md={4} className="cardHighlights p-3 m-3">
					  <Card.Body>
					    <Card.Title>Card Title</Card.Title>
					    <Card.Text>
					      Some quick example text to build on the card title and make up the bulk of
					      the card's content. Some quick example text to build on the card title and make up the bulk of
					      the card's content.
					    </Card.Text>
					  </Card.Body>
					</Card>
					<Card xs={12} md={4} className="cardHighlights p-3 m-3">
					  <Card.Body>
					    <Card.Title>Learn from Home</Card.Title>
					    <Card.Text>
					      Some quick example text to build on the card title and make up the bulk of
					      the card's content. Some quick example text to build on the card title and make up the bulk of
					      the card's content.Some quick example text to build on the card title and make up the bulk of
					      the card's content.
					    </Card.Text>
					  </Card.Body>
					</Card>
					<Card xs={12} md={4} className="cardHighlights p-3 m-3">
					  <Card.Body>
					    <Card.Title>Be a Part of  our Community</Card.Title>
					    <Card.Text>
					      Some quick example text to build on the card title and make up the bulk of
					      the card's content. Some quick example text to build on the card title and make up the bulk of
					      the card's content.
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>


	)
}