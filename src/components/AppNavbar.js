import { Navbar, Nav } from 'react-bootstrap';

export default function AppNavbar(){
	return (
		<Navbar bg="primary">
			<Navbar.Brand href="#">React Booking</Navbar.Brand>
			<Navbar.Toggle aria-conrols="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="me-auto">
					<Nav.Link href="#home">Home</Nav.Link>
					<Nav.Link href="courses">Courses</Nav.Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}

