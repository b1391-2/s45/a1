import { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';

export default function LogIn () {

	const [username, setUsername] = useState("")
	const [password, setPassword] = useState("")

	const [ islogInDisabled, setIsLogInDisabled] = useState(true) 

	useEffect(() => {
		if (username !== "" &&  password !== "") {
			setIsLogInDisabled(false)
		} else {
			setIsLogInDisabled(true)
		}
	}, [username, password])

	function logInUser(e) {
		e.preventDefault()
		alert(`User Logged in successfully!`);
		setUsername("");
		setPassword("");
	}
	return (
		<Container className="my-5">
			<Row className="justify-content-center">
				<Col xs={10} md={6}>
				<Form onSubmit={(e) => {
					logInUser(e)
				}}>
				  <Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control 
				    	type="email" 
				    	placeholder="Enter email"
				    	value={username}
				    	onChange={ (e) => {
				    		setUsername(e.target.value)
				    		//console.log(e);
				    		//console.log(email)
				    	}}
				    />
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="formBasicPassword">
				    <Form.Label>Password</Form.Label>
				    <Form.Control 
				    	type="password"
				    	placeholder="Password"
				    	onChange={(e) => {
				    		setPassword(e.target.value)
				    	}}
				    />
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="formBasicCheckbox">
				    <Form.Check type="checkbox" label="Check me out" />
				  </Form.Group>
				  <Button 
				  	variant="primary" 
				  	type="submit"
				  	disabled={islogInDisabled}
				  >
				    Submit
				  </Button>
				</Form>
				</Col>
			</Row>
		</Container>
	)
}