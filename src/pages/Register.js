import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function Register() {

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [confirmPassword, setConfirmPassword] = useState("")

	const [ islogInDisabled, setIsLogInDisabled] = useState(true) 

	useEffect( () => {
		if ((email !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) ){
			setIsLogInDisabled(false);
		} else {
			setIsLogInDisabled(true);
		}
	}, [email, password, confirmPassword])

	function registerUser(e) {
		e.preventDefault()

		setEmail("");
		setPassword("");
		setConfirmPassword("");

		alert(`User successfully registered.`)
	}

	return(
		<Container className="my-5">
			<Row className="justify-content-center">
				<Col xs={10} md={6}>
				<Form onSubmit={(e) => {
					registerUser(e)
				}}>
				  <Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control 
				    	type="email" 
				    	placeholder="Enter email"
				    	value={email}
				    	onChange={ (e) => {
				    		setEmail(e.target.value)
				    		//console.log(e);
				    		//console.log(email)
				    	}}
				    />
				    <Form.Text className="text-muted">
				      We'll never share your email with anyone else.
				    </Form.Text>
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="formBasicPassword">
				    <Form.Label>Password</Form.Label>
				    <Form.Control 
				    	type="password"
				    	placeholder="Password"
				    	onChange={ (e) => {
				    		setPassword(e.target.value)
				    	}}
				    />
				  </Form.Group>

				{/*Confirm Password*/}

				<Form.Group className="mb-3" controlId="formBasicConfirmPassword">
				  <Form.Label>Confirm Password</Form.Label>
				  <Form.Control 
				  	type="password"
				  	placeholder="Confirm Password"
				  	onChange={ (e) => {
				  		setConfirmPassword(e.target.value)
				  	}}
				  />
				</Form.Group>

				  <Form.Group className="mb-3" controlId="formBasicCheckbox">
				    <Form.Check type="checkbox" label="Check me out" />
				  </Form.Group>
				  <Button 
				  	variant="primary" 
				  	type="submit"
				  	disabled={islogInDisabled}

				  >
				    Submit
				  </Button>
				</Form>
				</Col>
			</Row>
		</Container>
	)
}