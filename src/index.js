import React from 'react';
import ReactDOM from 'react-dom';

/*Bootstrap StyleSheet*/
import 'bootstrap/dist/css/bootstrap.min.css';

//Import App.js file
import App from './App';


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)

